<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bookings as BookingsModel;

class Bookings extends Controller
{
    private $allowhost = 'river.kota-factory.com';
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    public function addbooking(Request $request){
        if($request->getHost() != $this->allowhost)
        {
            return response('Restricted', 400);
        }

        $validated = $request->validate([
            'name' => 'required|max:150',
            'message' => 'required',
            'bookingdate' => 'required'
        ]);

        $newbooking = BookingsModel::insert(
            array(
                'name' => filter_var($validated['name'], FILTER_SANITIZE_STRING),
                'bookings' => filter_var($validated['bookingdate'], FILTER_SANITIZE_STRING),
                'description' => filter_var($validated['message'], FILTER_SANITIZE_STRING),
                'status' => 2
            )
        );


        return response()->json([
            'success' => true,
            'data' => $newbooking
        ]);
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    public function getLastBookings(){
        $bookings = BookingsModel::from('bookings')
            ->select('name', 'description', 'bookings')
            ->limit(2)
            ->where('status', 2)
            ->orderBy('bookings', 'ASC')
            ->get();


        return response()->json([
            'success' => true,
            'data' => $bookings
        ]);
    }
}
