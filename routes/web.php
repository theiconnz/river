<?php
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::post('/api/bookings', [\App\Http\Controllers\Bookings::class, 'addbooking'])
    ->name('apibookings')->middleware('throttle:10:1');

Route::get('/api/getlastbookings', [\App\Http\Controllers\Bookings::class, 'getLastBookings'])
    ->name('lastbookings')->middleware('throttle:30:1');
